from tabulate import tabulate
from collections import OrderedDict

import numpy as np
import gevent

from bliss.config.static import get_config
from bliss.comm.spec.connection import SpecConnection, SpecClientNotConnectedError
from bliss.shell.getval import getval_yes_no


class SpecBliss:
    """
    Object that helps synchronizing axis offsets between SPEC and BLISS.


    - plugin: generic
      package: bm20.macros.spec
      class: SpecBliss
      name: sb_optics
      host: nolde  # host running SPEC
      session: dn  # name of the SPEC session
      axis:
      - m1: simot1  # SPEC mnemonic: BLISS mnemonic
      - simot2      # here the mnemonic is the same in SPEC and BLISS
      - s2t: slit2t


    
    """
    name = property(lambda self: self._name)

    def __init__(self, config):
        self._config = config
        self._name = config["name"]
        self._host = config["host"]
        self._session = config["session"]

        self._spec2bliss = OrderedDict()

        for axis in config.get('axis'):
            if isinstance(axis, (str,)):
                spec_mne = axis
                bliss_mne = spec_mne
            else:
                try:
                    axis = axis.to_dict()
                except AttributeError:
                    raise RuntimeError(f"{self.name}: wrong axis type "
                                        "(expected mapping or str).")
                if len(axis) != 1:
                    raise RuntimeError(f"TODO: invalid syntax in yaml: {axis}.")
                spec_mne, bliss_mne = list(axis.items())[0]
            
            if spec_mne in self._spec2bliss:
                raise RuntimeError(f"{self._name}: multiple occurences of SPEC mnemonic {spec_mne}.")
            if bliss_mne in self._spec2bliss.values():
                raise RuntimeError(f"{self._name}: multiple occurences of BLISS mnemonic {bliss_mne}.")

            self._spec2bliss[spec_mne] = bliss_mne

        address = f"{self._host}:{self._session}"
        self._connection = SpecConnection(address)

    def __info__(self):
        return self.state(_print=False)
        # txt = "===============\n"
        # txt += f"SpecBliss: {self.name}\n"
        # txt += f"Session:   {self._host}:{self._session}\n"
        # txt += "---------------\n\n"
        # tab_data = []
        # for spec_mne, bliss_mne in self._spec2bliss.items():
        #     tab_data += [[spec_mne, bliss_mne]]
        # txt += tabulate(tab_data,
        #                 headers=["spec", "bliss"])
        # txt += "\n===============\n"
        # return txt
    
    def state(self, _print=True):
        config = get_config()
        conn = self._connection
        txt = "===============\n"
        txt += f"SpecBliss: {self.name}\n"
        txt += f"Session:   {self._host}:{self._session}\n"
        txt += "---------------\n\n"
        tab_data = []
        failed = []
        try:
                for spec_mne, bliss_mne in self._spec2bliss.items():
                    try:
                        b_axis = config.get(bliss_mne)
                        b_ofst = b_axis.offset
                        s_ofst = conn.getChannel(f'motor/{spec_mne}/offset').read()
                        s_nlim = conn.send_msg_cmd_with_return(f"get_lim({spec_mne}, -1)")
                        gevent.sleep(0.1)
                        s_nlim = s_nlim.getValue()
                        s_plim = conn.send_msg_cmd_with_return(f"get_lim({spec_mne}, 1)")
                        gevent.sleep(0.1)
                        s_plim = s_plim.getValue()
                        tab_data += [[spec_mne, bliss_mne,
                                    s_ofst,
                                    b_ofst,
                                    s_ofst - b_ofst,
                                    s_nlim, s_plim,
                                    b_axis.limits[0], b_axis.limits[1]]]
                    except Exception as ex:
                        print(f"WARNING! Failed to get info from motor {spec_mne} in "
                            f"session {self._session} (exception: {ex})")
                        failed += [spec_mne]
                        tab_data += [[spec_mne, bliss_mne] + 5 * [np.nan]]
        except SpecClientNotConnectedError:
            raise RuntimeError(f"Can't connect to the SPEC session "
                               "{self._host}:{self._session}, is it running?")
        
        txt += "Current axis state:\n"
        txt += tabulate(tab_data,
                    #    showindex=self._spec2bliss.keys(),
                        headers=["spec", "bliss", "spec_ofst", "bliss_ofst",
                                 "ofst diff", "spec_lim-", "spec_lim+",
                                 "bliss_lim-", "bliss_lim+"],
                        floatfmt=".6f")
        txt += "\n===============\n"
        txt += "FAILED: " + '; '.join(failed)
        txt += "\n===============\n"
        if _print:
            print(txt)
        else:
            return txt

    def spec_ofst_to_bliss(self):
        config = get_config()
        conn = self._connection

        try:
            tab_data = []
            for spec_mne, bliss_mne in self._spec2bliss.items():
                b_ofst = config.get(bliss_mne).offset
                s_ofst = conn.getChannel(f'motor/{spec_mne}/offset').read()
                tab_data += [[spec_mne, bliss_mne,
                            s_ofst,
                            b_ofst,
                            s_ofst - b_ofst]]
        except SpecClientNotConnectedError:
            raise RuntimeError(f"Can't connect to the SPEC session "
                               "{self._host}:{self._session}, is it running?")
        print("The following offset(s) will be applied (SPEC to BLISS):")
        print(tabulate(tab_data,
                    #    showindex=self._spec2bliss.keys(),
                       headers=["spec", "bliss", "spec_ofst", "bliss_ofst", "diff"],
                       floatfmt=".6f"))
        
        if not getval_yes_no("Please confirm", default='n'):
            print("Canceled.")
            return
        print("Ok, setting offset(s)")

        # TODO: maybe check if the ofst has changed in the meantime?
        # like, use fell asleep before accepting...
        for spec_mne, bliss_mne, spec_ofst, bliss_ofst, _ in tab_data:
            bliss_axis = config.get(bliss_mne)
            bliss_axis.offset = spec_ofst
            print(f"OK, {bliss_mne} offset set to {bliss_axis.offset}... "
                  f"(from {spec_mne}, was {bliss_ofst}).")
            
    def spec_limits_to_bliss(self):
        config = get_config()
        conn = self._connection

        failed = []

        try:
            tab_data = []
            for spec_mne, bliss_mne in self._spec2bliss.items():
                try:
                    b_axis = config.get(bliss_mne)
                    b_nlim, b_plim = b_axis.limits
                    s_nlim = conn.send_msg_cmd_with_return(f"get_lim({spec_mne}, -1)")
                    gevent.sleep(0.1)
                    s_nlim = s_nlim.getValue()
                    s_plim = conn.send_msg_cmd_with_return(f"get_lim({spec_mne}, 1)")
                    gevent.sleep(0.1)
                    s_plim = s_plim.getValue()
                    tab_data += [[spec_mne, bliss_mne,
                                s_nlim, s_plim,
                                b_nlim, b_plim]]
                except Exception as ex:
                        print(f"WARNING! Failed to get info from motor {spec_mne} in "
                            f"session {self._session} (exception: {ex})")
                        failed += [spec_mne]
                        # tab_data += [[spec_mne, bliss_mne] + 5 * [np.nan]]
        except SpecClientNotConnectedError:
            raise RuntimeError(f"Can't connect to the SPEC session "
                               "{self._host}:{self._session}, is it running?")
        print("The following limits will be applied (SPEC to BLISS):")
        print(tabulate(tab_data,
                    #    showindex=self._spec2bliss.keys(),
                       headers=["spec", "bliss", "spec_lim-", "spec_lim+",
                                "bliss_lim-", "bliss_lim+"],
                       floatfmt=".6f"))
        
        if not getval_yes_no("Please confirm", default='n'):
            print("Canceled.")
            return
        print("Ok, setting limits")

        # TODO: maybe check if the ofst has changed in the meantime?
        # like, use fell asleep before accepting...
        for spec_mne, bliss_mne, s_nlim, s_plim, b_nlim, b_plim in tab_data:
            if (s_nlim == 0 and s_plim == 0) or not np.isfinite([s_nlim]):
                s_nlim = None
            if (s_nlim == 0 and s_plim == 0) or not np.isfinite([s_plim]):
                s_plim = None
            bliss_axis = config.get(bliss_mne)
            bliss_axis.limits = s_nlim, s_plim
            print(f"OK, {bliss_mne} offset set to {bliss_axis.limits}... "
                  f"(from {spec_mne}, was {(b_nlim, b_plim)}).")
        print("\n===============\n")
        print("FAILED: " + '; '.join(failed))
        print("\n===============\n")

    def bliss_ofst_to_spec(self):
        config = get_config()
        conn = self._connection
        
        try:
            tab_data = []
            for spec_mne, bliss_mne in self._spec2bliss.items():
                b_ofst = config.get(bliss_mne).offset
                s_ofst = conn.getChannel(f'motor/{spec_mne}/offset').read()
                tab_data += [[spec_mne, bliss_mne,
                            b_ofst,
                            s_ofst,
                            b_ofst - s_ofst]]
        except SpecClientNotConnectedError:
            raise RuntimeError(f"Can't connect to the SPEC session "
                               "{self._host}:{self._session}, is it running?")
        print("The following offset(s) will be applied (BLISS to SPEC):")
        print(tabulate(tab_data,
                    #    showindex=self._spec2bliss.keys(),
                       headers=["spec", "bliss", "bliss_ofst", "spec_ofst", "diff"],
                       floatfmt=".6f"))
        
        if not getval_yes_no("Please confirm", default='n'):
            print("Canceled.")
            return
        print("Ok, setting offset(s)")

        # TODO: maybe check if the ofst has changed in the meantime?
        # like, use fell asleep before accepting...
        for spec_mne, bliss_mne, bliss_ofst, spec_ofst, _ in tab_data:
            conn.getChannel(f'motor/{spec_mne}/offset').write(bliss_ofst)
            # TODO: check
            new_ofst = conn.getChannel(f'motor/{spec_mne}/offset').read()
            print(f"OK, {spec_mne} offset set to {new_ofst}... "
                  f"(from {bliss_mne}, was {spec_ofst}).")
            