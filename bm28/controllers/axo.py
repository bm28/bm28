"""
Bm28 Xray source controller.
https://confluence.esrf.fr/display/BM28KB/X-ray+Source
"""

from bliss.comm import get_comm
import gevent


class AxoCommand:
    def __init__(self, name, comm, cmd,
                 fmt_val=None, conv_ans=None,
                 n_lines=1):
        assert n_lines > 0
        self._name = name
        self._comm = comm
        self._cmd = cmd
        self._fmt_val = fmt_val
        self._conv_ans = conv_ans
        self._n_lines = n_lines

    def __call__(self, value=None):
        if value is not None:
            if self._fmt_val is not None:
                value = self._fmt_val(value)
            cmd = f"{self._cmd}{value}"
        else:
            cmd = self._cmd
        cmd += "\r\n"
#        print(">", cmd)
        self._comm.write(cmd.encode())
#        reply = [12.2]

        reply = [self._comm.readline(timeout=5).decode() for _ in range(self._n_lines)]
        gevent.sleep(0.05)
    #    print("<", reply)
        if reply[0] == "Com error":
            raise RuntimeError(f"Axo {self._name}: invalid syntax ({cmd})")
        if self._n_lines == 1:
            reply = reply[0]
        if self._conv_ans:
            reply = self._conv_ans(reply)
        return reply

# Rd_KV _ Read applied KV 
# Rd_MA _ Read Applied mA
# Wr_MA _ Send new mA setpoint
# Wr_KV _ Send new kV setpoint
# Version _ Emebedded software version
# Rd_SS _ Read generator status
# Xray_On _ Set Xray On 
# SHxx (ou xx représente le code shutter en hexa) _ Open/close the shutter
# Rd_WT _ Read Cooling fluid temperature
# Rd_WF _ Read cooling fluid flow
    

def _format_4_1f(data):
    return f"{data:04.1f}"

def _parse_status(data):
    #print(data)
    status, kv, ma = data
    status = int(status[2:])
    labels = ["sh_open", "xray_on", "temp_alarm_hi", "manual_mode", "temp_alarm_lo", "", "safety_loop_closed"]
    status = {label: status & 1 << bit != 0 for bit, label in enumerate(labels) if label}
    kv = float(kv[5:])
    ma = float(ma[5:])
    return status, kv, ma


class Axo:
    name = property(lambda self: self._name)
    config = property(lambda self: self._config)

    kv = property(lambda self: self._rd_kv())
    ma = property(lambda self: self._rd_ma())
    water_temp = property(lambda self: self._rd_wt())
    water_flow = property(lambda self: self._rd_wf())
    version = property(lambda self: self._version())
    status = property(lambda self: self._status())

    def __init__(self, config):
        self._config = config
        self._name = config["name"]
        com_cfg = config["tcp"]
        com_cfg["eol"] = "\r\n"
        self._comm = get_comm({"tcp": com_cfg})

        self._rd_kv = AxoCommand(self.name, self._comm,
                                 "Rd_KV", conv_ans=lambda data: float(data[5:]))
        self._rd_ma = AxoCommand(self.name, self._comm,
                                 "Rd_MA", conv_ans=lambda data: float(data[5:]))
        self._wr_kv = AxoCommand(self.name, self._comm, "Wr_KV", 
                                 fmt_val=_format_4_1f,
                                 conv_ans=lambda data: float(data[5:]))
        self._wr_ma = AxoCommand(self.name, self._comm, "Wr_MA",
                                 fmt_val=_format_4_1f,
                                 conv_ans=lambda data: float(data[5:]))
        self._version = AxoCommand(self.name, self._comm, "Version")
        self._status = AxoCommand(self.name, self._comm, "Rd_SS", conv_ans=_parse_status,
                                  n_lines=3)
        self._xon = AxoCommand(self.name, self._comm, "Xray_On")
        self._rd_wt = AxoCommand(self.name, self._comm,
                                 "Rd_WT", conv_ans=lambda data: float(data[5:]))
        self._rd_wf = AxoCommand(self.name, self._comm,
                                 "Rd_WF", conv_ans=lambda data: float(data[5:]))
        self._shopen = AxoCommand(self.name, self._comm,
                                  "SH01", conv_ans=lambda data: None)
        self._shclose = AxoCommand(self.name, self._comm,
                                  "SH00", conv_ans=lambda data: None)

    @kv.setter
    def kv(self, kv):
        # TODO: wait until it's reached the setpoint?
        self._wr_kv(kv)

    @ma.setter
    def ma(self, ma):
        # TODO: wait until it's reached the setpoint?
        self._wr_ma(ma)

    def xon(self):
        self._xon()

    def xoff(self):
        self.kv = 0
        self.ma = 0

    def shopen(self):
        # TODO: wait until it's opened?
        self._shopen()

    def shclose(self):
        # TODO: wait until it's closed?
        self._shclose()

    def __info__(self):
        txt = ""
        txt += "==== X-Ray source ====\n"
        txt += f"> name:    {self.name}\n"
        txt += f"> version: {self.version}\n"
        txt += f"> address: {self._comm._host}\n"
        txt += '-----\n\n'
        txt += f"Voltage:    {self.kv:>5.1f} kV\n"
        txt += f"Current:    {self.ma:>5.1f} mA\n"
        txt += f"Water temp: {self.water_temp} C"
        txt += f"Water flow: {self.water_flow}"
        txt += '-----\n\n'
        txt += "Status:\n"
        status, skv, sma = self.status
        txt += f"  - {'kV':<18s} {skv} kV\n"
        txt += f"  - {'mA':<18s} {sma} mA\n"
        txt += "  - "
        txt += "\n  - ".join([f"{key:<18s} : {value}" for key, value in status.items()])
        txt += '\n-----\n\n'
        txt += "Alarms:"
        has_alarm = False
        for alarm in ("temp_alarm_hi", "temp_alarm_lo"):
            if status[alarm]:
                txt += f"\n - {alarm}"
                has_alarm = True
        if not has_alarm:
            txt += " None\n"
        return txt
