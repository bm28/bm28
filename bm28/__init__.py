# -*- coding: utf-8 -*-

"""Top-level package for BM28 project."""

__author__ = """BCU Team"""
__email__ = 'bliss@esrf.fr'
__version__ = '0.1.0'
