ftimescan_mprg_data = """

UNSIGNED STARTTIME
UNSIGNED NPTS
UNSIGNED PERIOD
UNSIGNED ACQTIME

UNSIGNED ACCNB
UNSIGNED ACCTIME

UNSIGNED MODE
// MODE = 0 : acquisition master in time
// MODE = 1 : acquisition slave with itrig
// MODE = 2 : acquisition in time started on itrig
UNSIGNED SIGNAL
// SIGNAL = 0 : trig with itrig edges
// SIGNAL = 1 : trig with itrig levels

UNSIGNED IPT
UNSIGNED IACC
UNSIGNED LASTTIME

$DATA_ALIAS$

SUB STORE_INIT
    STORELIST TIMER $DATA_STORE$
    EMEM 0 AT 0
ENDSUB

SUB ACQ_IN_TIME
    FOR IPT FROM 1 TO NPTS
        AT DEFEVENT DO ATRIG BTRIG STORE
        LASTTIME = $TIMER

        IF (ACCNB > 1) THEN
            FOR IACC FROM 2 TO ACCNB
               @TIMER = $TIMER + ACCTIME
               AT TIMER DO ATRIG
            ENDFOR
        ENDIF
       
        @TIMER= LASTTIME + ACQTIME
        AT TIMER DO BTRIG STORE

        @TIMER= LASTTIME + PERIOD
        DEFEVENT TIMER
    ENDFOR

    AT TIMER DO STORE
ENDSUB

SUB ACQ_WITH_EDGE
    AT TIMER DO ATRIG BTRIG STORE
    EVSOURCE ITRIG FALL
    IF (ACCNB > 1) THEN
        FOR IACC FROM 2 TO ACCNB
            AT ITRIG DO NOTHING
        ENDFOR
    ENDIF
    AT ITRIG DO BTRIG STORE

    FOR IPT FROM 2 TO NPTS
        EVSOURCE ITRIG RISE
        AT ITRIG DO ATRIG BTRIG STORE
        PERIOD = $TIMER - LASTTIME
        LASTTIME = $TIMER
        EVSOURCE ITRIG FALL
        IF (ACCNB > 1) THEN
            FOR IACC FROM 2 TO ACCNB
                AT ITRIG DO NOTHING
            ENDFOR
        ENDIF
        AT ITRIG DO BTRIG STORE
    ENDFOR
    @TIMER = LASTTIME + PERIOD
    AT TIMER DO STORE
ENDSUB

SUB ACQ_WITH_LEVEL
    AT TIMER DO ATRIG BTRIG STORE
    EVSOURCE ITRIG HIGH
    AT ITRIG DO NOTHING
    IF (ACCNB > 1) THEN
        FOR IACC FROM 2 TO ACCNB
            EVSOURCE ITRIG LOW
            AT ITRIG DO NOTHING
            EVSOURCE ITRIG HIGH
            AT ITRIG DO NOTHING
        ENDFOR
    ENDIF
    EVSOURCE ITRIG LOW
    AT ITRIG DO BTRIG STORE

    FOR IPT FROM 2 TO NPTS
        EVSOURCE ITRIG HIGH
        AT ITRIG DO ATRIG BTRIG STORE
        PERIOD = $TIMER - LASTTIME
        LASTTIME = $TIMER
        IF (ACCNB > 1) THEN
            FOR IACC FROM 2 TO ACCNB
                EVSOURCE ITRIG LOW
                AT ITRIG DO NOTHING
                EVSOURCE ITRIG HIGH
                AT ITRIG DO NOTHING
            ENDFOR
        ENDIF
        EVSOURCE ITRIG LOW
        AT ITRIG DO BTRIG STORE
    ENDFOR
    @TIMER = LASTTIME + PERIOD
    AT TIMER DO STORE
ENDSUB

PROG FTIMESCAN

    GOSUB STORE_INIT

    BTRIG 0

    CTSTOP TIMER
    CTRESET TIMER
    CTSTART TIMER

    IF (MODE == 0) THEN
       DEFEVENT TIMER
       @TIMER= STARTTIME
       GOSUB ACQ_IN_TIME
    ELSEIF (MODE == 2) THEN
       EVSOURCE ITRIG RISE
       DEFEVENT ITRIG
       GOSUB ACQ_IN_TIME
    ELSE
       @TIMER= STARTTIME
       IF (SIGNAL == 0) THEN
          GOSUB ACQ_WITH_EDGE
       ELSE
          GOSUB ACQ_WITH_LEVEL
       ENDIF
    ENDIF
ENDPROG

PROG FTIMESCAN_CLEAN
    CTSTOP TIMER
    TIMER = 0
    BTRIG 0
ENDPROG

"""
