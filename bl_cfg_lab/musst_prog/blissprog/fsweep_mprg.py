fsweep_mprg_data = """
//****************************************************************
// FSWEEP :
//****************************************************************
// Description:
//   The program generates NPULSES gates on OutB starting from TRIGSTART.
//   Gate width is given by TRIGDELTA.
//   Second gate is generated exactly at end position of previous one.
//   Data are stored at rising and falling edge of each gate.
//   Scanning can be done in both direction. Direction is given by
//   sign of TRIGDELTA
//****************************************************************
//
// OutA     |
//          |_________
// OutB     |        |
//          |        |
// Pos  --------------------------------------->
//          ^
//       TRIGSTART
//
// OutA              |
//                   |_________
// OutB              |        |
//                   |        |
// Pos  --------------------------------------->
//                   |<------>|
//                    TRIGDELTA
//
//      ... repeated NPULSES times ...
//
// Buffer   *        *
//                   *        *
//                            *       * ...
//****************************************************************

// --- INPUT VARIABLES
SIGNED TRIGSTART
SIGNED TRIGDELTA
UNSIGNED NPULSES
UNSIGNED POSERR
UNSIGNED UNDERSHOOT

// --- INTERNAL VARIABLES
SIGNED SCANDIR
SIGNED LAST_TG
SIGNED NEXT_TG
UNSIGNED CUMERR
SIGNED POSCORR
SIGNED BACKDELTA

// --- OUTPUT VARIABLES
UNSIGNED NPOINTS

// --- PROGRAM ALIASES
$DATA_ALIAS$

SUB STORE_INIT
    STORELIST TIMER $DATA_STORE$
    EMEM 0 AT 0
ENDSUB


// --- init event source
SUB SET_NORMAL_DIR
    DEFEVENT MOT1
    IF (SCANDIR > 0) THEN
        EVSOURCE MOT1 UP
    ELSE
        EVSOURCE MOT1 DOWN
    ENDIF
ENDSUB

// --- inverse event direction
SUB SET_INVERT_DIR
    IF (SCANDIR > 0) THEN
        EVSOURCE MOT1 DOWN
    ELSE
        EVSOURCE MOT1 UP
    ENDIF
ENDSUB

// --- increment target position
// --- and correct error if needed
SUB INC_TARGET
    NEXT_TG = LAST_TG + TRIGDELTA

    CUMERR += POSERR
    IF ((CUMERR & 0x80000000) != 0) THEN
        CUMERR &= 0x7FFFFFFF
        NEXT_TG += POSCORR
    ENDIF

    LAST_TG = NEXT_TG
    @MOT1= NEXT_TG
ENDSUB

// --- main program loop
PROG FSWEEP
    // --- reset Trigger Out B signal
    BTRIG 0

    // --- reset timer
    CTSTOP TIMER
    TIMER = 0
    CTSTART TIMER

    // --- init store values
    GOSUB STORE_INIT

    // --- init variables
    NPOINTS = 0
    CUMERR = 0
    LAST_TG = TRIGSTART
    NEXT_TG = TRIGSTART

    IF (TRIGDELTA < 0) THEN
        SCANDIR= -1
        POSCORR= -1
        BACKDELTA= UNDERSHOOT
    ELSE
        SCANDIR= 1
        POSCORR= 1
        BACKDELTA= -UNDERSHOOT
    ENDIF

    // --- init event and first target
    GOSUB SET_NORMAL_DIR
    @MOT1 = NEXT_TG

    // --- main loop
    WHILE (NPOINTS < NPULSES) DO
        AT DEFEVENT DO STORE BTRIG ATRIG
        GOSUB INC_TARGET
        AT DEFEVENT DO STORE BTRIG
        IF (NPOINTS < NPULSES-1) THEN
            GOSUB SET_INVERT_DIR
            @MOT1= LAST_TG + BACKDELTA
            AT DEFEVENT DO NOTHING
            GOSUB SET_NORMAL_DIR
            @MOT1= LAST_TG
        ENDIF

        NPOINTS += 1
    ENDWHILE

    EXIT NPOINTS
ENDPROG

// --- cleanup function
PROG FSWEEP_CLEAN
    CTSTOP TIMER
    TIMER = 0

    BTRIG 0
ENDPROG

"""
