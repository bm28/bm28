
from bliss.setup_globals import *

load_script("gpib.py")

print("")
print("Welcome to your new 'gpib' BLISS session !! ")
print("")
print("You can now customize your 'gpib' session by changing files:")
print("   * /gpib_setup.py ")
print("   * /gpib.yml ")
print("   * /scripts/gpib.py ")
print("")