
from bliss.setup_globals import *

load_script("simu.py")

print("")
print("Welcome to your new 'simu' BLISS session !! ")
print("")
print("You can now customize your 'simu' session by changing files:")
print("   * /simu_setup.py ")
print("   * /simu.yml ")
print("   * /scripts/simu.py ")
print("")