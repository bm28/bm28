
from bliss.setup_globals import *

from bliss.common.hkl import *

load_script("fourc.py")

print("")
print("Welcome to your new 'fourc' BLISS session !! ")
print("")
print("You can now customize your 'fourc' session by changing files:")
print("   * /fourc_setup.py ")
print("   * /fourc.yml ")
print("   * /scripts/fourc.py ")
print("")