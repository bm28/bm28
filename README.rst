============
BM28 project
============

[![build status](https://gitlab.esrf.fr/BM28/bm28/badges/master/build.svg)](http://BM28.gitlab-pages.esrf.fr/BM28)
[![coverage report](https://gitlab.esrf.fr/BM28/bm28/badges/master/coverage.svg)](http://BM28.gitlab-pages.esrf.fr/bm28/htmlcov)

BM28 software & configuration

Latest documentation from master can be found [here](http://BM28.gitlab-pages.esrf.fr/bm28)
